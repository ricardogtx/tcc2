import time
import multiprocessing
import random

class Sensor(multiprocessing.Process):

    def __init__(self, name, sensor_queue):
        multiprocessing.Manager.__init__(self)
        self.name = name
        self.sensor_queue = sensor_queue

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self,name):
        self.__name = name

    def up(self):
        print("LOG_SENSOR: Subindo processo do Sensor nome:",self.__name,'\n')
        print("LOG_SENSOR: Após certificar que não há erro\n",
                "Conecta com o hardware do sensor")
        return True

    def read_sensor(self):
        number_sensor = []
        for i in range(10):
            ran = random.randint(1, 1000)
            number_sensor.append(ran)

        return number_sensor

    def data_validation(self, data):
        print("LOG_SENSOR: Entrou no data_validation\n")
        valid_number = []

        for i in data:
            if i % 2 == 0:
                valid_number.append(i)
        try:
            print("LOG_SENSOR: Colocando na sensor_queue\n", valid_number)
            self.sensor_queue.put(valid_number)
        except Exception as inst:
            print(inst)

        return

    def run(self):

        if self.up() == True:
            while True:
                print("LOG_SENSOR: Entrou no while\n")

                data = self.read_sensor()
                print("LOG_SENSOR: Leu o sensor\n")

                self.data_validation(data)
                print("LOG_SENSOR: Onde mexe com a queue\n")

                time.sleep(1)
                #break
        else:
            print("LOG_SENSOR: Error connecting to hardware\n")
