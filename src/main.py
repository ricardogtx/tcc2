import multiprocessing
import sensor, process, view

def subindo_sensor(sensor_queue,shared_list):

    try:
        print("LOG_MAIN: Subindo sensor\n")
        sens = sensor.Sensor("Sens",sensor_queue)
        sens.run()
    except Exception as inst:
        print(inst)
        print("LOG_MAIN: Falha ao instanciar módulo do sensor")



    return

def subindo_processamento(sensor_queue, process_queue, shared_list):

    try:
        print("LOG_MAIN: Subindo processamento\n")
        proc = process.Process(1,"Process",sensor_queue,process_queue)
        proc.run()
    except Exception as inst:
        print(inst)
        print("LOG_MAIN: Falha ao instanciar módulo do processamento")

    return

def subindo_visualizacao(process_queue, shared_list):

    try:
        print("LOG_MAIN: Subindo visualização\n")
        visu = view.View("Visu",process_queue)
        visu.run()
    except Exception as inst:
        print(inst)
        print("LOG_MAIN: Falha ao instanciar módulo de visualização")

    return

def main():

    # create a manager - it lets us share native Python object types like
    # lists and dictionaries without worrying about synchronization -
    # the manager will take care of it
    manager = multiprocessing.Manager()

    # now using the manager, create our shared data structures
    #odd_queue = manager.Queue()
    print("LOG_MAIN: Queue\n")
    sensor_queue = manager.Queue()
    process_queue = manager.Queue()
    view_queue = manager.Queue()

    shared_list = manager.list()

    #array_exemplo=[-1,-2,-3]
    #sensor_queue.put(array_exemplo)

    # lastly, create our pool of workers - this spawns the processes,
    # but they don't start actually doing anything yet
    print("LOG_MAIN: Pool\n")
    pool = multiprocessing.Pool()

    # now we'll assign two functions to the pool for them to run -
    # one to handle even numbers, one to handle odd numbers
    try:
        sensor_result = pool.apply_async(subindo_sensor, (sensor_queue, shared_list))
        process_result = pool.apply_async(subindo_processamento, (sensor_queue, process_queue, shared_list))
        view_result = pool.apply_async(subindo_visualizacao, (process_queue, shared_list))
    except Exception as inst:
        print(inst)
    #odd_result = pool.apply_async(subir_visualizar, (odd_queue, shared_list))

    # this code doesn't do anything with the odd_result and even_result
    # variables, but you have the flexibility to check exit codes
    # and other such things if you want - see docs for AsyncResult objects

    # now that the processes are running and waiting for their queues
    # to have something, lets give them some work to do by iterating
    # over our data, deciding who should process it, and putting it in
    # their queue
    #for i in range(10):

     #   if (i % 2) == 0: # use mod operator to see if "i" is even
        #    even_queue.put(i)

      #  else:
       #     odd_queue.put(i)

    # now we've finished giving the processes their work, so send the
    # poison pill to tell them to exit
    #even_queue.put(POISON_PILL)
    #odd_queue.put(POISON_PILL)

    # wait for them to exitpyt
    pool.close()
    pool.join()

    return


if __name__ == "__main__":
    main()
