import time
import multiprocessing

class View(multiprocessing.Process):

    def __init__(self, name, process_queue):
        multiprocessing.Manager.__init__(self)
        self.name = name
        self.process_queue = process_queue

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self,name):
        self.__name = name

    def up(self):
        print("LOG_VIEW: Subindo processo de Visualizacao nome:",self.__name,'\n')
        return True

    def print_motion(self):
        print("LOG_VIEW: Recebe dados tratados das juntas\n",
                "Converte para linguag  em da exibição e  apresenta o movimento\n")

        process_list = self.convert_language()
        print('*' * 30,"PRINT: Shared list:",process_list,'\n')


    def convert_language(self):
        print('LOG_VIEW: Converte dados tratados das juntas para linguagem alvo\n')

        if not self.process_queue.empty():
            #For print values and put again in queue
            process_list =[]
            for q in iter(self.process_queue.get, None):
                process_list.append(q)
                print("Q:",process_list,'\n')
                break
            #for l in process_list:
            #    self.process_queue.put(l)

            return process_list
        else:
            return None



    def run(self):
        if self.up() == True:
            while True:
                print("LOG_VIEW: Entrou no while\n")
                self.print_motion()
                time.sleep(2)
        else:
            print("PRINT: Error initializing view\n")
