from pandas import HDFStore,DataFrame
import multiprocessing

class Database(multiprocessing.Process):

    def __init__(self, mode, name, process_queue, storage='storage.h5'):
        multiprocessing.Manager.__init__(self)
        self.mode = mode
        self.name = name
        self.process_queue = process_queue
        self.storage = storage

    def up(self):
        print("Subindo processo de Banco nome:",self.__name, '\n')

    def convert_data(self):
        print('Converte dado tratado para padrão do banco\n')

    def save_data(self):
        print('Faz a lógica de aramazenamento e salva\n')

    def send_data(self):
        pass

    def run(self):
        while True:
            # mode that saves in the database
            if self.mode == 1:
                self.convert_data()
                self.save_data()

            # mode that compares to database
            elif self.mode == 2:
                send_data()

            else:
                print("Error to choose mode\n")
