import scipy.io as sio

def loadMat(path):
"""This function return numpy structured array"""

    file = sio.loadmat(path)
    key = list(file.keys())
    key.sort()
    struct = file[key[0]]
    return struct

def saveMat(name,dict):
"""This function save a file .mat"""

    sio.savemat('output/' + name +'.mat',{name:dict})
