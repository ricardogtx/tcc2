import time
import multiprocessing

class Process(multiprocessing.Process):

    def __init__(self, mode, name, sensor_queue, process_queue):
        multiprocessing.Manager.__init__(self)
        self.name = name
        self.process_queue = process_queue
        self.sensor_queue = sensor_queue
        self.mode = mode

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self,name):
        self.__name = name

    def processes_data(self):
        print("LOG_PROCESS: Processes data\n")
        if not self.sensor_queue.empty():
            try:
                data = self.sensor_queue.get()
                print("LOG_PROCESS: Tentando pegar na fila\n")
            except Exception as inst:
                print(inst)

            new_data = self.convert_output_data(data)

            try:
                self.process_queue.put(new_data)
            except Exception as inst:
                print(inst)


    def convert_output_data(self,data):
        new_data = []
        for i in data:
            new_data.append(i*100)

        return new_data

    def compare_database(self):
        return 'Compara movimento com o movimento do banco de dados'

    def run(self):
        if self.mode == 1:
            while True:
            #mode that saves in the database
                print("LOG_PROCESS: Entrou no modo\n")

                self.processes_data()
                print("LOG_PROCESS: Processou o dado\n")


                #break

                process_list =[]
                for q in iter(self.process_queue.get, None):
                    process_list.append(q)
                    print(20 *'*',"LOG_PROCESS:",process_list)
                    break
                for l in process_list:
                   self.process_queue.put(l)

                   time.sleep(2)

                #For print all attributes
                #self.process_queue = dir(__builtins__)
                #print(self.process_queue)
                #break

            #mode that compares to database
        elif mode == 2:
            while True:
                self.processes_data()
                self.compare_database()


            else:
                print("LOG_PROCESS: Error to choose mode\n")
